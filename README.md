# ddns

采用阿里云做域名解析服务器的动态域名系统

## Usage

```
go run main.go aliyun_access_keyid=<aliyun_access_keyid> aliyun_access_secret=<aliyun_access_secret> domains=<subdomain1>,<subdomain2> interval=1
```