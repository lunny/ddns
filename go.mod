module gitea.com/lunny/ddns

go 1.12

require (
	code.gitea.io/log v0.0.0-20190608101221-52ddba89623c
	gitea.com/lunny/config v0.0.0-20190521063422-1ff82d55983d
	gitea.com/lunny/ip138 v0.0.0-20190614065532-47fd1fd0812c
	github.com/aliyun/alibaba-cloud-sdk-go v0.0.0-20190605052313-b394c08122fc
	github.com/pkg/errors v0.8.1
)
