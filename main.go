package main

import (
	"os"
	"strings"
	"time"

	"code.gitea.io/log"
	"gitea.com/lunny/config"
	"gitea.com/lunny/ip138"
	"github.com/aliyun/alibaba-cloud-sdk-go/services/alidns"
	"github.com/pkg/errors"
)

var (
	aliyunAccessKeyId  string
	aliyunAccessSecret string
	domains            []string
	aliyunRegion       = "cn-hangzhou"
	interval           = 0 // minutes, if zero then immediately, otherwise every interval time update once.
)

func settings(configPath string) error {
	_, err := os.Stat(configPath)
	if err != nil && !os.IsNotExist(err) {
		return errors.Wrap(err, "os.Stat")
	}

	cfgIsNotExist := os.IsNotExist(err)

	var cfg *config.Config
	if !cfgIsNotExist {
		cfg, err = config.Load(configPath)
		if err != nil {
			if !os.IsNotExist(err) {
				return errors.Wrap(err, "config.Load(configPath)")
			}
		}
	}

	if cfg == nil {
		cfg = config.New(config.LoadEnvs(), config.LoadFlags())
	}

	aliyunAccessKeyId = cfg.MustString("aliyun_access_keyid", aliyunAccessKeyId)
	aliyunAccessSecret = cfg.MustString("aliyun_access_secret", aliyunAccessSecret)
	domains = cfg.GetSlice("domains", ",")
	aliyunRegion = cfg.MustString("aliyun_region", aliyunRegion)
	interval = cfg.MustInt("interval", 0)
	return nil
}

// splitDomain return the subpath and subdomain's parent domain
func splitDomain(subDomain string) (string, string) {
	s := strings.Split(subDomain, ".")
	if strings.HasSuffix(subDomain, ".com.cn") ||
		strings.HasSuffix(subDomain, ".net.cn") {
		return strings.Join(s[:len(s)-3], "."), strings.Join(s[len(s)-3:], ".")
	}
	return strings.Join(s[:len(s)-2], "."), strings.Join(s[len(s)-2:], ".")
}

func updateDomain() {
	ipAddr, err := ip138.ExternalIP()
	if err != nil {
		log.Error("ip138 failed: %v", err)
		return
	}

	log.Info("external IP is %v", ipAddr)

	client, err := alidns.NewClientWithAccessKey(aliyunRegion, aliyunAccessKeyId, aliyunAccessSecret)
	if err != nil {
		log.Error("NewClientWithAccessKey faild: %v", err)
		return
	}

	for _, domain := range domains {
		subPath, parentDomain := splitDomain(domain)
		req1 := alidns.CreateDescribeDomainRecordsRequest()
		req1.DomainName = parentDomain
		response1, err := client.DescribeDomainRecords(req1)
		if err != nil {
			log.Error("Update domain %s to %s failed: %v", domain, ipAddr, err)
			continue
		}

		var theRecord *alidns.Record
		for _, record := range response1.DomainRecords.Record {
			if record.RR == subPath {
				theRecord = &record
				break
			}
		}

		if theRecord != nil {
			if theRecord.Value != ipAddr {
				req2 := alidns.CreateUpdateDomainRecordRequest()
				req2.RecordId = theRecord.RecordId
				req2.RR = subPath
				req2.Value = ipAddr
				req2.Type = "A"
				_, err := client.UpdateDomainRecord(req2)
				if err != nil {
					log.Error("Update domain %s to %s failed: %v", domain, ipAddr, err)
					continue
				}
			} else {
				log.Info("Domain %s's IP is %s and not changed", domain, theRecord.Value)
				continue
			}
		} else {
			req3 := alidns.CreateAddDomainRecordRequest()
			req3.DomainName = parentDomain
			req3.RR = subPath
			req3.Value = ipAddr
			req3.Type = "A"
			_, err := client.AddDomainRecord(req3)
			if err != nil {
				log.Error("Update domain %s to %s failed: %v", domain, ipAddr, err)
				continue
			}
		}

		log.Info("Update domain %s to %s successfully", domain, ipAddr)
	}
}

func main() {
	if err := settings("./config.ini"); err != nil {
		log.Fatal("%v", err)
	}

	if aliyunAccessKeyId == "" || aliyunAccessSecret == "" || len(domains) == 0 {
		log.Fatal("ddns aliyun_access_keyid=<aliyun_access_keyid> aliyun_access_secret=<aliyun_access_secret> domains=<domains>")
	}

	if interval == 0 {
		updateDomain()
		return
	}

	for {
		updateDomain()

		time.Sleep(time.Duration(interval) * time.Minute)
	}

}
